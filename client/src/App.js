import { useEffect, useState, Fragment } from 'react';
import axios from 'axios';

import { LoadingIcon } from './LoadingIcon';
import { EditIcon } from './EditIcon';
import { JiraIcon } from './JiraIcon';
import './App.css';
import {HeartIcon} from "./HeartIcon";

const STORAGE_KEY = 'prd_info';

function App() {
  const [inputText, setInputText] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isLoadingTasks, setIsLoadingTasks] = useState([]);
  const [createdTaskIndexes, setCreatedTaskIndexes] = useState([]);
  const [tasks, setTasks] = useState([]);
  const [editableItems, setEditableItems] = useState({});

  const handleInputChange = (event) => {
    setInputText(event.target.value);
  };

  function resetState() {
    setIsLoadingTasks([]);
    setTasks([]);
    setEditableItems({});
  }

  const handleFormSubmit = (event) => {
    event.preventDefault();

    if (inputText && !isLoading) {
      resetState();
      saveIntoStorage([]);
      setIsLoading(true);
      axios.post('http://localhost:3001/api/processText', { text: inputText })
        .then((response) => {
          setTasks(response.data.tasks);
          setIsLoading(false);
          saveIntoStorage(response.data.tasks);
        })
        .catch((error) => {
          setIsLoading(false);
          setTasks([]);
          console.error(error);
        });
    }
  };

  function saveIntoStorage(tasks) {
    localStorage.setItem(STORAGE_KEY, JSON.stringify({
      inputText,
      tasks,
    }));
  }

  const handleEditClick = (index, task) => {
    setEditableItems(oldState => ({
      ...oldState,
      [index]: {
        ...task,
      },
    }))
  };

  const handleTaskCreate = (index) => {
    if (!isLoadingTasks.includes(index) && !createdTaskIndexes.includes(index)) {
      setIsLoadingTasks(oldList => [...oldList, index]);
      axios.post('http://localhost:3001/api/jira/create', { tasks: [tasks[index]] })
        .then((response) => {
          setCreatedTaskIndexes(oldList => [...oldList, index]);
          setIsLoadingTasks(oldList => oldList.filter(item => item !== index));
        })
        .catch((error) => {
          setIsLoadingTasks(oldList => oldList.filter(item => item !== index));
          console.error(error);
        });
    }
  };

  const handleCreateTasks = () => {
    if (!isLoadingTasks.length) {
      const newTaskEntries = Object.entries(tasks).filter(([i]) => !createdTaskIndexes.includes(i));
      const indexes = newTaskEntries.map(item => +item[0]);
      setIsLoadingTasks(oldList => [...oldList, ...indexes]);
      axios.post('http://localhost:3001/api/jira/create', { tasks: newTaskEntries.map(item => item[1]) })
        .then((response) => {
          setCreatedTaskIndexes(oldList => [...oldList, ...newTaskEntries.map(item => +item[0])]);
          setIsLoadingTasks(oldList => oldList.filter(item => !indexes.includes(item)));
        })
        .catch((error) => {
          setIsLoadingTasks(oldList => oldList.filter(item => !indexes.includes(item)));
          console.error(error);
        });
    }
  };

  const handleSaveClick = (index) => {
    if (editableItems[index].title && editableItems[index].description) {
      setTasks((prevTasks) => prevTasks.map((task, i) => (i === index ? editableItems[index] : task)));
      setEditableItems(oldState => ({
        ...oldState,
        [index]: null,
      }));
    }
  };

  const handleEditTitle = (index, title) => {
    setEditableItems(oldState => ({
      ...oldState,
      [index]: {
        ...oldState[index],
        title,
      },
    }));
  };

  const handleEditDescription = (index, description) => {
    setEditableItems(oldState => ({
      ...oldState,
      [index]: {
        ...oldState[index],
        description,
      },
    }));
  };

  useEffect(() => {
    const storageData = localStorage.getItem(STORAGE_KEY);

    if (storageData) {
      try {
        const data = JSON.parse(storageData);

        if (data.inputText) {
          setInputText(data.inputText)

          setTasks(data.tasks || []);
        }


      } catch (err) {
        console.error(err);
      }
    }
  }, []);

  return (
  <>
      <div className="container">
        <form onSubmit={handleFormSubmit}>
          <label htmlFor="inputText">Input Text:</label>
          <textarea
            placeholder="Enter your PRD here..."
            onChange={handleInputChange}
            value={inputText}
            maxLength={2000}
            id="inputText"
            rows={8}
          />
          <div className="action-buttons">
            <button type="submit" className="submitButton">
              {isLoading ? (
                <><LoadingIcon /> &nbsp;&nbsp;Please Wait </>
              ) : 'Submit'}
            </button>
            {tasks.length > createdTaskIndexes.length ? (
              <button
                onClick={() => handleCreateTasks()}
                className="jiraButton"
                type="button"
              >
                {isLoadingTasks.length ? (
                  <HeartIcon />
                ) : (
                  <JiraIcon />
                )}
              </button>
            ) : null}
          </div>
        </form>
      </div>
    <div className="board">
      {tasks.map((task, index) => (
          <div
            className={`card ${editableItems[index] ? 'editing' : ''} ${createdTaskIndexes.includes(index) ? 'created-task' : ''}`}
            key={index}
          >
            {editableItems[index] ? (
                <>
                  <div className="input-container">
                    <input
                        type="text"
                        value={editableItems[index].title}
                        onChange={(e) => handleEditTitle(index, e.target.value)}
                    />
                    <textarea
                        value={editableItems[index].description}
                        onChange={(e) => handleEditDescription(index, e.target.value)}
                        rows={4}
                    />
                  </div>
                  <div className="save-btn-container">
                    <button onClick={() => handleSaveClick(index)}>Save</button>
                  </div>
                </>
            ) : (
                <>
                  <div className="card-title">
                    {task.title.split('\n').map((line, index) => (
                      <Fragment key={index}>
                        {line}
                        <br />
                      </Fragment>
                    ))}
                  </div>
                  <div className="card-description">
                    {task.description.split('\n').map((line, index) => (
                      <Fragment key={index}>
                        {line}
                        <br />
                      </Fragment>
                    ))}
                  </div>
                  {createdTaskIndexes.includes(index) ? null : (
                    <>
                      <button className="action-btn edit-btn" onClick={() => handleEditClick(index, task)}><EditIcon /></button>
                      <button
                        onClick={() => handleTaskCreate(index)}
                        className="action-btn jira-btn"
                      >
                        {isLoadingTasks.includes(index) ? (
                          <HeartIcon />
                        ) : (
                        <JiraIcon />
                        )}
                      </button>
                    </>
                  )}
                </>
            )}
          </div>
      ))}
    </div>
  </>
  );
}

export default App;
