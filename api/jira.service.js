// jira.js
require('dotenv').config();
const axios = require('axios');

const createTaskInJira = async (task) => {
    const jiraProjectId = process.env.PROJECT_ID;

    const data = {
        fields: {
            project: {
                id: jiraProjectId
            },
            summary: task.title,
            description: {
                type: 'doc',
                version: 1,
                content: [
                    {
                        type: 'paragraph',
                        content: [
                            {
                                text: task.description,
                                type: 'text'
                            }
                        ]
                    }
                ]
            },
            issuetype: {
                id: 3
            }
        }
    };

    try {
        const response = await axios({
            method: 'post',
            url: process.env.JIRA_API_URL,
            headers: {
                'Authorization': `Basic ${Buffer.from(`${process.env.JIRA_EMAIL}:${process.env.JIRA_API_TOKEN}`).toString('base64')}`,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            data
        });

        return response.data;

    } catch (error) {
        console.error(error.response);
        throw new Error('An error occurred while creating the ticket');
    }
};

module.exports = createTaskInJira;
