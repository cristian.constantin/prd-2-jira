// server.js
require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const axios = require('axios');

const { promptSuffix } = require('./constants');
const createTaskInJira = require('./jira.service');

const app = express();
app.use(bodyParser.json());
app.use(cors());


app.post('/api/processText', async (req, res) => {
  const prdText = req.body.text;

  const suffix = promptSuffix;

  try {
    // Call to the OpenAI API
    const response = await axios.post('https://api.openai.com/v1/chat/completions', {
      model: 'gpt-4-32k',
      "messages": [{ "role": "user", "content": `${prdText}. ${suffix}` }],
      temperature: 0
    }, {
      headers: {
        Authorization: `Bearer ${process.env.OPENAI_API_KEY}`,
        "Content-Type": 'application/json'
      }
    })

    const completion = response.data.choices[0].message.content;

    const tasks = splitTasks(completion)

    res.json({ tasks });
  } catch (error) {
    console.error(error);
    res.status(500).send('Error processing request');
  }
});

app.post('/api/jira/create', async (req, res) => {
  const tasks = req.body.tasks;

  try {
    const taskResponses = await Promise.all(tasks.map(createTaskInJira));

    res.json({ tasks: taskResponses });
  } catch (error) {
    console.error(error);
    res.status(500).send('Error creating Jira tasks');
  }
});

function splitTasks(text) {
  const rawTasks = text.split('\n\n');

  const tasks = rawTasks.map(rawTask => {
      const lines = rawTask.split('\n');

      const title = lines[0].replace(/Task \d+: /, '');

      const summaryIndex = lines.findIndex(line => line.startsWith('Summary:'));
      const descriptionIndex = lines.findIndex(line => line.startsWith('Description:'));
      const acceptanceCriteriaIndex = lines.findIndex(line => line.startsWith('Acceptance Criteria:'));

      const summary = summaryIndex !== -1 ? lines[summaryIndex].replace('Summary: ', '') : '';
      const rawDescription = descriptionIndex !== -1 ? lines[descriptionIndex].replace('Description: ', '') : '';
      const acceptanceCriteria = acceptanceCriteriaIndex !== -1 ? lines[acceptanceCriteriaIndex] : '';

      const description = `${summary}\n${rawDescription}\n${acceptanceCriteria}`;

      return { title, description };
  });

  return tasks;
}

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
